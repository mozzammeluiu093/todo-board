# To clone the project

# Using SSH

`git clone git@gitlab.com:mozzammeluiu093/todo-board.git`

After clone the project, there are two directories frontend and backend <br>

# To Setup Backend

`cd backend`

then, follow README.md file inside backend directory <br>

Backend project build laravel with latest version composer. So make sure your machine has composer <br>

# To Setup Frontend

`cd frontend`

then, follow README.md file inside frontend directory <br>

Frontend project build vite react with latest version node. So make sure your machine has node <br>



