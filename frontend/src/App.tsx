import { BrowserRouter } from "react-router-dom";
import { Layout } from "./components/layouts/layout";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
function App() {
  return (
    <BrowserRouter>
      <Layout />
      <ToastContainer />
    </BrowserRouter>
  );
}
export default App;
