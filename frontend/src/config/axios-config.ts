import axios from 'axios';
import { baseURL } from '../constant/constant';

const instance = axios.create({
  baseURL: baseURL,
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${localStorage.getItem("_token")}`,
  },
});

instance.interceptors.request.use(
  (config) => {
    // Modify config or do something before request is sent
    const token = localStorage.getItem("_token");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
// Response interceptor for handling 401 errors
instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error?.response && error?.response?.data?.message === 'Token has expired') {
      localStorage.removeItem('_token')
      window.location.replace('/')
    }
    return Promise.reject(error);
  }
);

export default instance;