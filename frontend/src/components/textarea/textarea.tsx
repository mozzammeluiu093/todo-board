/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import styles from "./style.module.scss";
export const Textarea = ({
  id,
  name,
  label,
  placeholder,
  autofocus,
  register,
  rules,
  error,
  message,
}: any) => {
  return (
    <div className={styles.container}>
      <label className={styles.container__label}>
        {label}
        <textarea
          className={styles.container__input}
          autoFocus={autofocus}
          id={id}
          {...register(name, rules)}
          placeholder={placeholder}
        />
      </label>
      {error && <p className={styles.container__error_message}>{message}</p>}
    </div>
  );
};
