import axios from "axios";
import styles from "./style.module.scss";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
const AppBar = () => {
  const navigate = useNavigate();
  const handleLogout = () => {
    const token = localStorage.getItem("_token");
    axios
      .post("http://127.0.0.1:8000/api/logout", {token})
      .then((response) => {
        const { message } = response.data;
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
        localStorage.removeItem("_token");
        navigate("/");
      })
      .catch((error) => {
        const { message } = error?.response?.data;
        toast.error(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };
  return (
    <div className={styles.container}>
      <div className={styles.container__title}>
        <div className={styles.container__title__first}>
         ToDo
        </div>
        <div className={styles.container__title__second}>
         Board
        </div>
        <div className={styles.container__title__home} onClick={() => navigate('/dashboard')}>Home</div>
        <div className={styles.container__title__history} onClick={() => navigate('/dashboard/history')}>History</div>
        <div>
        </div>
      </div>
      <div className={styles.container__right}>
        <button
          className={styles.container__right__button}
          onClick={() => handleLogout()}
        >
          Logout
        </button>
      </div>
    </div>
  );
};

export default AppBar;
