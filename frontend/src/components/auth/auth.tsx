/* eslint-disable @typescript-eslint/no-unsafe-return */
import { Navigate } from "react-router-dom";
export const Auth = ({ children }: any) => {
  const token = localStorage.getItem("_token") || null;
  const isUnAuthenticatePage =
    location?.pathname?.startsWith("/registration") ||
    location?.pathname === "/";
  if (token === null && !isUnAuthenticatePage) {
    return <Navigate to="/" />;
  } else if (token !== null && isUnAuthenticatePage) {
    return <Navigate to="/dashboard" />;
  }
  return children;
};
