import { useLocation } from "react-router-dom";
import { AppLayout } from "./app-layout";
import { DashboardLayout } from "./dashboard-layout";

export const Layout = () => {
  const location = useLocation();
  const isDashboard = location?.pathname?.startsWith("/dashboard");
  return isDashboard ? <DashboardLayout /> : <AppLayout />;
};
