/* eslint-disable @typescript-eslint/no-unsafe-call */
import { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import { Routers } from "../../router/router";
import { Auth } from "../auth/auth";

export const AppLayout = () => {
  const WrapComponent = (Component: any, props: any) => (
    <Component {...props} />
  );
  return (
    <Suspense fallback={"loading"}>
      <Routes location={location}>
        {Routers.map(({ path, component: Component }: any) => (
          <Route
            key={path as string}
            element={<Auth>{WrapComponent(Component, {})}</Auth>}
            path={path as string}
          />
        ))}
      </Routes>
    </Suspense>
  );
};
