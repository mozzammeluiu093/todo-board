/* eslint-disable @typescript-eslint/no-unsafe-call */
import { Suspense } from "react";
import { Route, Routes } from "react-router-dom";
import { Auth } from "../auth/auth";
import { DashboardRouters } from "../../router/dashboard-route";
import AppBar from "../app-bar/app-bar";

export const DashboardLayout = () => {
  const WrapComponent = (Component: any, props: any) => {
    return (
      <>
        <AppBar />
        <Component {...props} />
      </>
    );
  };
  return (
    <Suspense fallback={"loading"}>
      <Routes location={location}>
        {DashboardRouters.map(({ path, component: Component }: any) => (
          <Route
            key={path as string}
            element={<Auth>{WrapComponent(Component, {})}</Auth>}
            path={path as string}
          />
        ))}
      </Routes>
    </Suspense>
  );
};
