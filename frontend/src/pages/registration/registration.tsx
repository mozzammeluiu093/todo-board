/* eslint-disable @typescript-eslint/no-misused-promises */
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { Input } from "../../components/input/input";
import styles from "./style.module.scss";
import axios from "axios";
import { toast } from "react-toastify";
export const Registration = () => {
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const handleRegistration = (values: any) => {
    axios
      .post("http://127.0.0.1:8000/api/register", values)
      .then((response) => {
        const { success, message } = response.data;
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
        success && navigate('/');
      }).catch((error) => {
        const {message} = error?.response?.data
        toast.error(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };
  return (
    <div className={styles.container}>
      <div className={styles.container__content}>
        <form
          className={styles.container__content__form}
          onSubmit={handleSubmit((data) => {
            handleRegistration(data);
          })}
        >
          <div className={styles.container__content__form__heading}>
            Create Account
          </div>
          <div className={styles.container__content__form__inputs}>
            <Input
              type="text"
              id="name"
              name="name"
              label="Name*"
              placeholder="John Doe"
              autofocus={true}
              register={register}
              error={!!errors?.name}
              message={errors?.name?.message}
              rules={{
                required: {
                  value: true,
                  message: "Please Enter Name",
                },
                maxLength: {
                  value: 255,
                  message: "Maximum 255 Character",
                },
              }}
            />
            <Input
              type="email"
              id="email"
              name="email"
              label="Email Address*"
              placeholder="me@example.com"
              autofocus={false}
              register={register}
              error={!!errors?.email}
              message={errors?.email?.message}
              rules={{
                required: {
                  value: true,
                  message: "Please Enter Email",
                },
                minLength: {
                  value: 3,
                  message: "Minimum 3 Character",
                },
              }}
            />

            <Input
              type="password"
              id="password"
              name="password"
              label="Password*"
              placeholder="••••••••••"
              register={register}
              error={!!errors?.password}
              message={errors?.password?.message}
              rules={{
                required: {
                  value: true,
                  message: "Please Enter Password",
                },
                maxLength: {
                  value: 6,
                  message: "Maximum 6 Character",
                },
                minLength: {
                  value: 6,
                  message: "Minimum 6 Character",
                },
              }}
            />
            <div className={styles.container__content__form__inputs__register}>
              <p>
                Already have an account?{" "}
                <span
                  className={
                    styles.container__content__form__inputs__register__text
                  }
                  onClick={() => navigate("/")}
                >
                  Login
                </span>
              </p>
            </div>
            <button
              type="submit"
              className={styles.container__content__form__inputs__submit_btn}
            >
              Submit
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
