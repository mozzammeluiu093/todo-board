import { useForm } from "react-hook-form";
import styles from "./style.module.scss";
import Modal from "react-modal";
import { useState } from "react";
import { Input } from "../../../components/input/input";
import axios from "../../../config/axios-config";
import { toast } from "react-toastify";
const customStyles = {
  content: {
    zIndex: "100",
    width: "50%",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};
export interface IAddLabelProps {
  setLabels: (value: any) => void;
}
export const AddLabel = ({ setLabels }: IAddLabelProps) => {
  Modal.setAppElement("#root");
  const [modalIsOpen, setIsOpen] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
    resetField,
  } = useForm();
  const closeModal = () => {
    setIsOpen(false);
  };
  const onSubmit = (values: any) => {
    axios
      .post("/api/ticket/label/create", values)
      .then((response) => {
        const {
          message,
          data: { name, id },
        } = response.data;
        setLabels((prev: any) => [
          ...prev,
          {
            id,
            name,
          },
        ]);
        resetField("name");
        closeModal();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      })
      .catch((error) => {
        const { status } = error?.response;
        const message = status === 422 ? 'Duplicate name not allowed' : 'Bad Requst' 
        toast.warning(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };
  return (
    <div>
      <button
        className={styles.container__btn}
        onClick={() => {
          setIsOpen(true);
        }}
      >
        + Add Another List
      </button>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Task Input"
      >
        <div className={styles.container__modal}>
          <div className={styles.container__modal__header}>
            Add Label
          </div>
          <button className={styles.container__modal__close_btn} onClick={closeModal}>
            Close
          </button>
        </div>
        <hr/>
        <br></br>
        <form
          onSubmit={handleSubmit((values: any) => {
            onSubmit(values);
          })}
        >
          <Input
            type="text"
            id="name"
            name="name"
            label="Category / Label*"
            placeholder="ToDo"
            autofocus={true}
            register={register}
            error={!!errors?.name}
            message={errors?.name?.message}
            rules={{
              required: {
                value: true,
                message: "Please Enter Label Name",
              },
              minLength: {
                value: 2,
                message: "Minimum 2 Character",
              },
            }}
          />
          <button className={styles.container__modal__submit_btn} type="submit">
            Submit
          </button>
        </form>
      </Modal>
    </div>
  );
};
