import { AddTicket } from "./add-ticket/add-ticket";
import { EditTicket } from "./edit-ticket/edit-ticket";
import styles from "./style.module.scss";
export const Column = ({
  ticketLabelId,
  title,
  column,
  cards,
  dragEnter,
  dragLeave,
  allowDrop,
  drop,
  drag,
  setCards,
}: any) => {
  return (
    <div
      className={styles.container__board__column}
      data-column={column}
      onDragEnter={dragEnter}
      onDragLeave={dragLeave}
      onDragOver={allowDrop}
      onDrop={(event) => drop(event, column, ticketLabelId)}
    >
      <div className={styles.container__board__column__header}>
        <div className={styles.container__board__column__header__title}>
          {title}
        </div>
        <div className={styles.container__board__column__header__add_ticket}>
          <AddTicket ticketLabelId={ticketLabelId} setCards={setCards} />
        </div>
      </div>
      <hr />
      {cards
        .filter((card: any) => card.label.name === title)
        .map((todo: any) => (
          <div
            key={todo.id}
            className={styles.container__board__column__card}
            draggable="true"
            onDragStart={drag}
            data-id={todo.id}
          >
            <div className={styles.container__board__column__header}>
              <div className={styles.container__board__column__header__title}>
                {todo.title}
              </div>
              <div
                className={styles.container__board__column__header__add_ticket}
              >
                <EditTicket ticketLabelId={ticketLabelId} defaultValue={todo} setCards={setCards} />
              </div>
            </div>
            <hr />
            <div className={styles.container__board__column__card__description}>
              {todo.description ||
                "Description: Lorem ipsum dolor sit amet, consectetur adipiscingelit."}
            </div>
            <div className={styles.container__board__column__card__expiry_date}>
              {`Expires on: ${todo.expiry_date ? todo.expiry_date : '-'}`}
            </div>
          </div>
        ))}
    </div>
  );
};
