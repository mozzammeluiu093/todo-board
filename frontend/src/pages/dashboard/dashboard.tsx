import { useEffect, useState } from "react";
import styles from "./style.module.scss";
import { useDragAndDrop } from "./drag-and-drop-hook";
import { Column } from "./column";
import axios from '../../config/axios-config';
import { AddLabel } from "./add-label/add-label";

export const Dashboard = () => {
  const [cards, setCards] = useState([]);
  const [labels, setLabels] = useState([]);
  const { dragEnter, dragLeave, drag, drop, allowDrop } = useDragAndDrop(
    cards,
    setCards
  );

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [ticketListResponse, ticketLabelsResponse] = await Promise.all([
          axios.get(`/api/tickets`),
          axios.get(`/api/ticket/labels`),
        ]);
  
        const ticketListData = ticketListResponse.data;
        const ticketLabelsData = ticketLabelsResponse.data;
        setCards(ticketListData);
        setLabels(ticketLabelsData);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);
  return (
    <div className={styles.container}>
      <div className={styles.container__add}>
      <AddLabel setLabels = {setLabels}/>
      </div>
      <main className={styles.container__board}>
        {labels.map(({name:labelName,id}: any) => (
          <Column
            key={labelName}
            ticketLabelId={id}
            title={labelName}
            column={labelName}
            cards={cards}
            setCards={setCards}
            {...{ dragEnter, dragLeave, allowDrop, drop, drag }}
          />
        ))}
      </main>
    </div>
  );
};