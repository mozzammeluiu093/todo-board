import { useEffect } from "react";
import axios from "../../config/axios-config";

export const useDragAndDrop = (cards: any, setCards: any) => {
  const dragEnter = (event: any) => {
    event.currentTarget.classList.add("drop");
  };

  const dragLeave = (event: any) => {
    event.currentTarget.classList.remove("drop");
  };

  const drag = (event: any) => {
    event.dataTransfer.setData("text/plain", event.currentTarget.dataset.id);
  };

  const drop = (event: any, column: any, ticketLabelId: number) => {
    const id = Number(event.dataTransfer.getData("text/plain"));
    event.currentTarget.classList.remove("drop");
    event.preventDefault();
    const updatedState = cards.map((card: any) => {
      if (card.id === id) {
        card.label.name = column;
        axios.put(`/api/ticket/update/${id}`, {
          ...card,
          ticket_label_id: ticketLabelId,
        });
        const currentDate = new Date();
        const options: any = {
          month: "short",
          day: "2-digit",
          year: "numeric",
          hour: "2-digit",
          minute: "2-digit",
          hour12: false
        };

        const formattedDate = new Intl.DateTimeFormat("en-US", options).format(
          currentDate
        );

        const customFormattedDate = formattedDate.replace(/,/, "");
        const message = `${card.title}: has been moved to <strong>${column}</strong> on ${customFormattedDate}`;
        axios.post('/api/history/create',{message})
      }
      return card;
    });
    setCards(updatedState);
  };

  const allowDrop = (event: any) => {
    event.preventDefault();
  };

  const dragStart = (event: any) => {
    if (event.target.className.includes("card")) {
      event.target.classList.add("dragging");
    }
  };

  const dragEnd = (event: any) => {
    if (event.target.className.includes("card")) {
      event.target.classList.remove("dragging");
    }
  };

  useEffect(() => {
    document.addEventListener("dragstart", dragStart);
    document.addEventListener("dragend", dragEnd);

    return () => {
      document.removeEventListener("dragstart", dragStart);
      document.removeEventListener("dragend", dragEnd);
    };
  }, []);

  return { dragEnter, dragLeave, drag, drop, allowDrop };
};
