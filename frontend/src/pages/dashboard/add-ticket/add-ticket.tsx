import { useForm } from "react-hook-form";
import styles from "./style.module.scss";
import Modal from "react-modal";
import { useState } from "react";
import { Input } from "../../../components/input/input";
import axios from "../../../config/axios-config";
import { toast } from "react-toastify";
import { Textarea } from "../../../components/textarea/textarea";
const customStyles = {
  content: {
    zIndex: "100",
    width: "50%",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};
export interface IAddTicketProps {
  ticketLabelId: number;
  setCards: (value: any) => void;
}
export const AddTicket = ({ ticketLabelId, setCards }: IAddTicketProps) => {
  Modal.setAppElement("#root");
  const [modalIsOpen, setIsOpen] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
    resetField,
  } = useForm();
  const closeModal = () => {
    setIsOpen(false);
  };
  const onSubmit = (values: any) => {
    axios
      .post("/api/ticket/create", { ...values, ticket_label_id: ticketLabelId })
      .then((response) => {
        const { message, data } = response.data;
        console.log(message, data);
        setCards((prev: any) => [
          ...prev,
          {
            ...data,
          },
        ]);
        resetField("title", undefined);
        resetField("description", undefined);
        closeModal();
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      })
      .catch((error) => {
        const { status } = error?.response;
        const message =
          status === 422 ? "Duplicate name not allowed" : "Bad Requst";
        toast.warning(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };
  return (
    <div>
      <div
        className={styles.container__btn}
        onClick={() => {
          setIsOpen(true);
        }}
      >
        + Add
      </div>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Task Input"
      >
        <div className={styles.container__modal}>
          <div className={styles.container__modal__header}>Add Label</div>
          <button
            className={styles.container__modal__close_btn}
            onClick={closeModal}
          >
            Close
          </button>
        </div>
        <hr />
        <br></br>
        <form
          onSubmit={handleSubmit((values: any) => {
            onSubmit(values);
          })}
        >
          <Input
            type="text"
            id="title"
            name="title"
            label="Title*"
            placeholder="Ex: title"
            autofocus={true}
            register={register}
            error={!!errors?.title}
            message={errors?.title?.message}
            rules={{
              required: {
                value: true,
                message: "Please Enter Title",
              },
              minLength: {
                value: 2,
                message: "Minimum 2 Character",
              },
            }}
          />
          <Textarea
            id="description"
            name="description"
            label="Description*"
            placeholder="Ex: Lorem Ipsum"
            autofocus={false}
            register={register}
            error={!!errors?.description}
            message={errors?.description?.message}
            rules={{
              required: {
                value: true,
                message: "Please Enter Description",
              },
              minLength: {
                value: 2,
                message: "Minimum 2 Character",
              },
            }}
          />
          <Input
            type="date"
            id="date"
            name="expiry_date"
            label="Expiry Date*"
            placeholder="Ex: "
            autofocus={true}
            register={register}
            error={!!errors?.expiry_date}
            message={errors?.expiry_date?.message}
            rules={{
              required: {
                value: true,
                message: "Please Enter Expiry Date",
              }
            }}
          />
          <button className={styles.container__modal__submit_btn} type="submit">
            Submit
          </button>
        </form>
      </Modal>
    </div>
  );
};
