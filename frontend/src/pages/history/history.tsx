import { useEffect, useState } from "react";
import styles from "./style.module.scss";
import axios from "../../config/axios-config";
export const History = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      axios.get("/api/history").then((response) => {
        setData(response.data);
      });
    };
    fetchData();
  }, []);
  return (
    <div className={styles.container}>
      <div className={styles.container__content}>
        {data.map(({ message, id }: any) => (
          <div key={id} className={styles.container__content__element}>
            {" "}
            <p dangerouslySetInnerHTML={{ __html: message }} />
          </div>
        ))}
      </div>
    </div>
  );
};
