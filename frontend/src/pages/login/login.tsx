/* eslint-disable @typescript-eslint/no-misused-promises */
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { Input } from "../../components/input/input";
import styles from "./style.module.scss";
import axios from "axios";
import { toast } from "react-toastify";
export const Login = () => {
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const handleLogin = (values: any) => {
    axios
      .post("http://127.0.0.1:8000/api/login", values)
      .then((response) => {
        const { token, message } = response.data;
        localStorage.setItem('_token', token);
        toast.success(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
        navigate('/dashboard');
      }).catch((error) => {
        const {message} = error?.response?.data
        toast.error(message, {
          position: toast.POSITION.TOP_RIGHT,
        });
      });
  };
  return (
    <div className={styles.container}>
      <div className={styles.container__content}>
        <form
          className={styles.container__content__form}
          onSubmit={handleSubmit((data) => {
            handleLogin(data);
          })}
        >
          <div className={styles.container__content__form__heading}>Log In</div>
          <div className={styles.container__content__form__inputs}>
            <Input
              type="email"
              id="email"
              name="email"
              label="Email Address*"
              placeholder="me@example.com"
              autofocus={true}
              register={register}
              error={!!errors?.email}
              message={errors?.email?.message}
              rules={{
                required: {
                  value: true,
                  message: "Please Enter Email",
                },
                minLength: {
                  value: 3,
                  message: "Minimum 3 Character",
                },
              }}
            />

            <Input
              type="password"
              id="password"
              name="password"
              label="Password*"
              placeholder="••••••••••"
              register={register}
              error={!!errors?.password}
              message={errors?.password?.message}
              rules={{
                required: {
                  value: true,
                  message: "Please Enter Password",
                },
                maxLength: {
                  value: 6,
                  message: "Maximum 8 Character",
                },
                minLength: {
                  value: 6,
                  message: "Minimum 8 Character",
                },
              }}
            />
            <div className={styles.container__content__form__inputs__register}>
              <p>
                Don't have an account?{" "}
                <span
                  className={
                    styles.container__content__form__inputs__register__text
                  }
                  onClick={() => navigate("/registration")}
                >
                  Register
                </span>
              </p>
            </div>
            <button
              type="submit"
              className={styles.container__content__form__inputs__submit_btn}
            >
              Sign In
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};
