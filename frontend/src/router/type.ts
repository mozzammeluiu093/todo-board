import { RouteProps } from "react-router-dom";

export type IRouterType = RouteProps & {
  [key: string]: any;
};
