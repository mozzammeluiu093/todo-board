import { Login } from "../pages/login/login";
import { Registration } from "../pages/registration/registration";
import { IRouterType } from "./type";

export const Routers: IRouterType = [
  {
    path: "/",
    component: Login,
    exact: true,
  },
  {
    path: "/registration",
    component: Registration,
    exact: true,
  },
];
