
import { Dashboard } from "../pages/dashboard/dashboard";
import { History } from "../pages/history/history";
import { IRouterType } from "./type";

export const DashboardRouters: IRouterType = [
  {
    path: "/dashboard",
    component: Dashboard,
    exact: true,
  },
  {
    path: "/dashboard/history",
    component: History,
    exact: true,
  }
];
