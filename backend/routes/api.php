<?php

use App\Http\Controllers\TicketController;
use App\Http\Controllers\TicketLabelController;
use App\Http\Controllers\TodoHistoryController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'middleware' => 'api',
], function ($router) {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'authenticate']);

});

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('profile', [AuthController::class, 'get_user']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    //ticket label routes
    Route::get('ticket/labels', [TicketLabelController::class, 'index']);
    Route::post('ticket/label/create', [TicketLabelController::class, 'store']);
    Route::put('ticket/label/update/{label}',  [TicketLabelController::class, 'update']);
    //ticket routes
    Route::get('tickets', [TicketController::class, 'index']);
    Route::post('ticket/create', [TicketController::class, 'store']);
    Route::put('ticket/update/{ticket}',  [TicketController::class, 'update']);
    //history routes
    Route::get('history', [TodoHistoryController::class, 'index']);
    Route::post('history/create', [TodoHistoryController::class, 'store']);
});
