<?php

namespace App\Http\Controllers;

use App\Models\TicketLabel;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class TicketLabelController extends Controller
{
    protected $user;
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->user
            ->ticketLabels()
            ->select('id','name')
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return
     */
    public function store(Request $request)
    {
        $data = $request->only('name');
        $validator = Validator::make($data, [
            'name' => 'required|string|unique:ticket_labels,name,NULL,id,user_id,' . $this->user->id,
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //Request is valid, create new product
        $ticket_label = $this->user->ticketLabels()->create([
            'name' => $request->name,
        ]);

        //Product created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Label created successfully',
            'data' => $ticket_label
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TicketLabel  $ticketLabel
     * @return \Illuminate\Http\Response
     */
    public function show(TicketLabel $ticketLabel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TicketLabel  $ticketLabel
     * @return \Illuminate\Http\Response
     */
    public function edit(TicketLabel $ticketLabel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TicketLabel  $ticketLabel
     * @return
     */
    public function update(Request $request, TicketLabel $ticketLabel)
    {
        //Validate data
        $data = $request->only('name');
        $validator = Validator::make($data, [
            'name' => 'required|string'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //Request is valid, update product
        $product = $ticketLabel->update([
            'name' => $request->name
        ]);

        //Product updated, return success response
        return response()->json([
            'success' => true,
            'message' => 'Label updated successfully',
            'data' => $product
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TicketLabel  $ticketLabel
     * @return
     */
    public function destroy(TicketLabel $ticketLabel)
    {
        $ticketLabel->delete();
        return response()->json([
            'success' => true,
            'message' => 'Label deleted successfully'
        ], Response::HTTP_OK);
    }
}
