<?php

namespace App\Http\Controllers;

use App\Models\Ticket;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class TicketController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }
    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public function index()
    {
        return $this->user
            ->tickets()
            ->with('label')
            ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return
     */
    public function store(Request $request)
    {
        //Validate data
        $data = $request->only(
            'title',
            'description',
            'expiry_date',
            'ticket_label_id'
        );
        $validator = Validator::make($data, [
            'title' => 'required|string',
            'ticket_label_id' => 'required'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //Request is valid, create new ticket
        $ticket = $this->user->tickets()->create([
            'title' => $request->title,
            'description' => $request->description,
            'expiry_date' => $request->expiry_date,
            'ticket_label_id' => $request->ticket_label_id,
        ]);

        //Product created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Ticket created successfully',
            'data' => $ticket->with('label')->find($ticket->id)
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return
     */
    public function update(Request $request, Ticket $ticket)
    {
        //Validate data
        $data = $request->only(
            'title',
            'description',
            'expiry_date',
            'ticket_label_id'
        );
        $validator = Validator::make($data, [
            'title' => 'required|string',
            'ticket_label_id' => 'required'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //Request is valid, update ticket
        $ticket->update([
            'title' => $request->title,
            'description' => $request->description,
            'expiry_date' => $request->expiry_date,
            'ticket_label_id' => $request->ticket_label_id,
        ]);

        //Ticket updated, return success response
        return response()->json([
            'success' => true,
            'message' => 'Ticket updated successfully',
            'data' => $ticket->with('label')->find($ticket->id)
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
