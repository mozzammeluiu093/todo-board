<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;

class TodoHistoryController extends Controller
{
    protected $user;
    public function __construct()
    {
        $this->user = JWTAuth::parseToken()->authenticate();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->user
            ->todoHistory()
            ->select('id','message')
            ->latest()
            ->get();
    }

    public function store(Request $request)
    {
        $data = $request->only('message');
        $validator = Validator::make($data, [
            'message' => 'required|string',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //Request is valid, create new product
        $this->user->todoHistory()->create([
            'message' => $request->message,
        ]);

        //Product created, return success response
        return response()->json([
            'success' => true,
            'message' => 'History created successfully',
        ], Response::HTTP_OK);
    }
}
