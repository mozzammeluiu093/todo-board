<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $fillable = [
         'title',
         'description',
         'expiry_date',
         'ticket_label_id',
         'user_id'
    ];
    public function label()
    {
        return $this->hasOne(TicketLabel::class,'id','ticket_label_id');
    }
}
