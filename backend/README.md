# Laravel Project Setup

## Laravel Install Dependencies

To install project dependencies through composer<br/>

`composer install`

### Generate Key

First, copy .env.example file into same directory as .env. Then to generate app key <br>

`php artisan generate:key`

#### Database Setup

Then, to connect with mysql database. Fill the DB_PASSWORD with your mysql password.<br>

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=your-mysql-password

#### JWT Secret Generate

We use JWT token to use API authentication. To generate JWT Secret key, <br>

`php artisan jwt:secret`

which will add key in .env file<br>

JWT_SECRET

#### JWT TTL

By default, token expires in 60 minutes, To customize it, add below key and assign a value in minute<br>

JWT_TTL= 720

#### Database Migration

After, above configuration, we need to run migration, to run migration <br>

`php artisan migrate`

#### Run Project

Finally, To run project <br/>

`php artisan serve`

